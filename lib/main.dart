import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_3/src/app/app.dart';
import 'package:path_provider/path_provider.dart' as path_provider;


 void main() async {
   WidgetsFlutterBinding.ensureInitialized();
   final appDocDir = await path_provider.getApplicationDocumentsDirectory();
   await Firebase.initializeApp();
   Hive.init(appDocDir.path);
   await Hive.openBox('adult');
   await Hive.openBox('code');
   await Hive.openBox('locale');
   await Hive.openBox('weekDay');
   await Hive.openBox('region');
   await Hive.openBox('referrerAPI');
   await Hive.openBox('room');
   runApp(MyApp());
 }
///////////////////////////////////////////////////////////////////////////////////////
//For FireBase Remote Config

// void main() async{
//   WidgetsFlutterBinding.ensureInitialized();
//   final appDocDir = await path_provider.getApplicationDocumentsDirectory();
//    await Firebase.initializeApp();
//   Hive.init(appDocDir.path);
//    await Hive.openBox('adult');
//    await Hive.openBox('code');
//    await Hive.openBox('locale');
//    await Hive.openBox('weekDay');
//    await Hive.openBox('region');
//   runApp(MaterialApp(
//       title: 'Remote Config Example',
//       home: FutureBuilder<RemoteConfig>(
//         future: setupRemoteConfig(),
//         builder: (BuildContext context, AsyncSnapshot<RemoteConfig> snapshot) {
//           return snapshot.hasData
//               ? WelcomeWidget(remoteConfig: snapshot.data)
//               : Container();
//         },
//       )));
// }

// class WelcomeWidget extends AnimatedWidget {
//   WelcomeWidget({this.remoteConfig}) : super(listenable: remoteConfig);
//
//   final RemoteConfig remoteConfig;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Remote Config Example'),
//       ),
//       body: Center(child: Text('Welcome ${remoteConfig.getString('welcome')}')),
//       floatingActionButton: FloatingActionButton(
//           child: const Icon(Icons.refresh),
//           onPressed: () async {
//             try {
//               // Using default duration to force fetching from remote server.
//               await remoteConfig.fetch(expiration: const Duration(seconds: 0));
//               await remoteConfig.activateFetched();
//             } on FetchThrottledException catch (exception) {
//               // Fetch throttled.
//               print(exception);
//             } catch (exception) {
//               print(
//                   'Unable to fetch remote config. Cached or default values will be '
//                       'used');
//             }
//           }),
//     );
//   }
// }

// Future<RemoteConfig> setupRemoteConfig() async {
//   await Firebase.initializeApp();
//   final RemoteConfig remoteConfig = await RemoteConfig.instance;
//   // Enable developer mode to relax fetch throttling
//   remoteConfig.setConfigSettings(RemoteConfigSettings(debugMode: true));
//   remoteConfig.setDefaults(<String, dynamic>{
//     'welcome': 'welcome user',
//     'hello': 'default hello',
//   });
//   return remoteConfig;
// }
//////////////////////////////////////////////////////////////////////////////////////////////////
// class App extends StatelessWidget {
//   // Create the initialization Future outside of `build`:
//   final Future<FirebaseApp> _initialization = Firebase.initializeApp();

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: FutureBuilder(
//         // Initialize FlutterFire:
//         future: _initialization,
//         builder: (context, snapshot) {
//           // Check for errors
//           if (snapshot.hasError) {
//             return MyApp();
//           }

//           // Once complete, show your application
//           if (snapshot.connectionState == ConnectionState.done) {
//             return MyApp();
//           }

//           // Otherwise, show something whilst waiting for initialization to complete
//           return Scaffold(
//             body: Center(
//               child: Text('connecting to firebase...'),
//             ),
//           );
//         },
//       ),
//     );

//   }
// }
