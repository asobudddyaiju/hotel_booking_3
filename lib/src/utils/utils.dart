import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

///it contain common functions
class Utils {
  static String capitalize(String s) {
    if (s != null && s.isNotEmpty) {
      return s[0].toUpperCase() + s.substring(1);
    } else {
      return "";
    }
  }
}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

int numberOfDays(List<List<DateTime>> intervals) {
  DateTime startTime = DateTime(
      intervals[0][0].year, intervals[0][0].month, intervals[0][0].day);
  DateTime endTime = DateTime(
      intervals[0][1].year, intervals[0][1].month, intervals[0][1].day);
  int differenceInDays = endTime.difference(startTime).inDays + 1;
  Hive.box('adult').put(5, differenceInDays);
  print(differenceInDays.toString());
  return differenceInDays;
}

Future<Locale> getLocale() async {}

int initialCheckIn (List<List<DateTime>> intervals) {
  DateTime startTime = DateTime.now();
  DateTime endTime = DateTime(
      intervals[0][0].year, intervals[0][0].month, intervals[0][0].day);
  int differenceInDays = endTime.difference(startTime).inDays + 0;
  Hive.box('adult').put('checkIn', differenceInDays);
  print(differenceInDays.toString());
  return differenceInDays;
}
int initialCheckOut (List<List<DateTime>> intervals) {
  DateTime startTime = DateTime.now();
  DateTime endTime = DateTime(
      intervals[0][1].year, intervals[0][1].month, intervals[0][1].day);
  int differenceInDays = endTime.difference(startTime).inDays + 0;
  Hive.box('adult').put('checkOut', differenceInDays);
  print(differenceInDays.toString());
  return differenceInDays;
}
Future<String> getPublicIP() async {
  try {
    const url = 'https://api.ipify.org';
    var response = await http.get(url);
    // print(response.statusCode);
    // print(response.body);
    String ip = response.body.toString();

    try {
      String url = 'http://api.ipstack.com/$ip?access_key=d42ac7f928933d400d70d3a95590356b&format=1';
      var response = await http.get(url);
      // print(response.statusCode);
      // print(response.body);
      var data = json.decode(response.body);
      Hive.box('room').put('country', data["country_name"].toString());
      Hive.box('room').put('currency', data["currency"]["code"].toString());
      print( 'country='+ Hive.box('room').get('country'));
      print( 'country='+ Hive.box('room').get('currency'));
      return null;

    } catch (e) {
      print(e);
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}
// String months = Hive.box('code').get(5);
// String checkInMonth(String months){
//   String month1;
//   switch(months){
//     case '1':
//       month1 = 'Jan';
//       break;
//     case '2':
//       month1 = 'Jan';
//       break;
//     case '3':
//       month1 = 'Jan';
//       break;
//     case '4':
//       month1 = 'Jan';
//       break;
//     case '5':
//       month1 = 'Jan';
//       break;
//     case '6':
//       month1 = 'Jan';
//       break;
//     case '7':
//       month1 = 'Jan';
//       break;
//     case '8':
//       month1 = 'Jan';
//       break;
//     case '9':
//       month1 = 'Jan';
//       break;
//     case '10':
//       month1 = 'Jan';
//       break;
//     case '11':
//       month1 = 'Jan';
//       break;
//     case '12':
//       month1 = 'Jan';
//       break;
//     default:
//       month1 = '';
//       break;
//   }
//   Hive.box('code').put(7, month1);
// }
// String checkOutMonth(String month2){
//   String month2;
//   String month2s = Hive.box('code').get(6).toString();
//   switch(month2s){
//     case '1':
//       month2 = 'Jan';
//       break;
//     case '2':
//       month2 = 'Jan';
//       break;
//     case '3':
//       month2 = 'Jan';
//       break;
//     case '4':
//       month2 = 'Jan';
//       break;
//     case '5':
//       month2 = 'Jan';
//       break;
//     case '6':
//       month2 = 'Jan';
//       break;
//     case '7':
//       month2 = 'Jan';
//       break;
//     case '8':
//       month2 = 'Jan';
//       break;
//     case '9':
//       month2 = 'Jan';
//       break;
//     case '10':
//       month2 = 'Jan';
//       break;
//     case '11':
//       month2 = 'Jan';
//       break;
//     case '12':
//       month2 = 'Jan';
//       break;
//     default:
//       month2 = '';
//       break;
//   }
//   return month2;
//   Hive.box('code').put(8, month2);
// }
