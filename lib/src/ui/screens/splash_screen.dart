import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_3/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_3/src/utils/constants.dart';
import 'package:hotel_booking_3/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  Timer _timerControl;
  AnimationController animationController;
  Animation animIconHeight;
  Animation animIconWidth;
  Animation animIconColor;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
              (route) => false);
    });
  }

  @override
  void didChangeDependencies() {
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animIconColor = ColorTween(
        begin: Constants.kitGradients[0], end: Constants.kitGradients[3])
        .animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconHeight = Tween<double>(
      end: 40.0,
      begin: 80.0,
    ).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconWidth = Tween<double>(end: 40.0, begin: 100.0).animate(
        CurvedAnimation(
            parent: animationController, curve: Interval(0.5, 0.6)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    startTimer();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    animationController.dispose();
    _timerControl.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              backgroundColor: Constants.kitGradients[12],
              elevation: 0.0,
            )),
        body: Builder(
            builder: (context) => SafeArea(
                top: true,
                left: true,
                bottom: true,
                child: Stack(children: [
                  Container(
                    height: screenHeight(context, dividedBy: 1),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Constants.kitGradients[12],
                            Constants.kitGradients[12],
                            Constants.kitGradients[12].withOpacity(.99),
                            Constants.kitGradients[12].withOpacity(.98),
                            Constants.kitGradients[12].withOpacity(.98),
                            Constants.kitGradients[12].withOpacity(.95),
                            Constants.kitGradients[12].withOpacity(.90),
                            Constants.kitGradients[12].withOpacity(.85),
                            Constants.kitGradients[12].withOpacity(.80),
                            Constants.kitGradients[12].withOpacity(.75),
                            Constants.kitGradients[12].withOpacity(.65),
                            Constants.kitGradients[12].withOpacity(.60),
                            Constants.kitGradients[12].withOpacity(.55),
                            Constants.kitGradients[12].withOpacity(.50),
                            Constants.kitGradients[12].withOpacity(.45),
                            Constants.kitGradients[12].withOpacity(.40)
                          ]),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 4),
                        ),
                        Container(
                          height: 150,
                          width: 150,
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: animIconHeight.value,
                              bottom: animIconHeight.value,
                              left: animIconWidth.value,
                              right: animIconWidth.value,
                            ),
                            child: SvgPicture.asset(
                              "assets/images/app_icon_final.svg",
                              fit: BoxFit.fill,
                              color: Constants.kitGradients[3],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Shimmer.fromColors(
                            child: Text(
                              'last minute hotels',
                              style: TextStyle(
                                  fontFamily: 'Josefin',
                                  fontSize: 28,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal),
                            ),
                            baseColor: Constants.kitGradients[3],
                            highlightColor: Constants.kitGradients[4]),
                      ],
                    ),
                  ),
                ]))));
  }
}
