import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_3/localization/locale.dart';
import 'package:hotel_booking_3/src/auth_service/firebase_auth.dart';
import 'package:hotel_booking_3/src/ui/screens/currency.dart';
import 'package:hotel_booking_3/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_3/src/ui/screens/region.dart';
import 'package:hotel_booking_3/src/ui/widgets/build_dropcurrency.dart';
import 'package:hotel_booking_3/src/ui/widgets/build_dropdown.dart';
import 'package:hotel_booking_3/src/ui/widgets/profile_list_tile.dart';
import 'package:hotel_booking_3/src/utils/object_factory.dart';
import 'package:hotel_booking_3/src/utils/utils.dart';

import 'login.dart';

class ProfilePage extends StatefulWidget {
  FirebaseAuth auth;
  FirebaseAuth authfb;
  ProfilePage({this.auth,this.authfb});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';
  String defaultLang = Platform.localeName;

  @override
  void initState() {
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 20)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
            ),
            title: Text(
              'Profile',
              style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.normal),
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 15),
          ),
          Card(
            elevation: 0.5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right: screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 75)),
                      child: Text(
                        getTranslated(context, 'language'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 75),
                          right: screenWidth(context, dividedBy: 50),
                          left: screenWidth(context, dividedBy: 50)),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegionPage()));
                        },
                        child: Text(
                          Hive.box('lang').get(2) != null
                              ? Hive.box('lang').get(2)
                              : ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1],
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 15),
                          right:screenWidth(context, dividedBy: 15),
                          top: screenHeight(context, dividedBy: 100)),
                      child: Text(
                        getTranslated(context, 'Currency'),
                        style: TextStyle(fontSize: 14, color: Colors.black),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 75),
                            right: screenWidth(context, dividedBy: 50),
                            left: screenWidth(context, dividedBy: 50),
                            bottom: screenHeight(context, dividedBy: 75)),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CurrencyPage()));
                          },
                          child: Text(
                            Hive.box('code').get(2) != null
                                ? Hive.box('code').get(2).toString().split(" ").first
                                : Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                            overflow: TextOverflow.visible,
                            style: TextStyle(fontSize: 14),
                          ),
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: screenHeight(context, dividedBy: 15)),
          Card(
            elevation: 0.5,
            child: Column(children: [
              ProfileListTile(
                listText: 'About us',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Help and feedback',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Term and conditions',
                listText1: '',
              ),
              ProfileListTile(
                listText: 'Privacy policy',
                listText1: '',
              ),
              GestureDetector(
                onTap: () {
                  if ((widget.auth.currentUser == null) &&
                      (widget.authfb.currentUser == null)) {
                    // Navigator.pushAndRemoveUntil(
                    //  context,
                    //  MaterialPageRoute(
                    //      builder: (context) => Login(auth: widget.auth)),
                    //  (route) => false);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                Login(auth: widget.auth, authFb: widget.authfb)));
                  } else {
                    void signout() async {
                      await Auth(auth: widget.auth, authfb: widget.authfb)
                          .Signout();
                    }

                    signout();

                    setState(() {
                      buttonName = "Sign in";
                      photpurl = dummyphotourl;
                      displayName = dummydisplayName;
                    });
                  }
                },
                child: ProfileListTile(
                  listText: buttonName,
                  listText1: '',
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
