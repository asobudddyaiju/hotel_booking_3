import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_3/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_3/src/utils/constants.dart';
import 'package:hotel_booking_3/src/utils/utils.dart';

class CurrencyPage extends StatefulWidget {
  @override
  _CurrencyPageState createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  List<String> currency = [];
  String value;
  @override
  void initState() {
    setState(() {
      currency = currencies;
    });
    setState(() {
      value = Hive.box('code').get(2);
    });
    super.initState();
  }

  List<String> currencies = [
    "AED",
    "BGN",
    "BRL",
    "CNY",
    "CZK",
    "HRK",
    "DKK",
    "EUR",
    "HUF",
    "HKD",
    "ISK",
    "IDR",
    "ILS",
    "JPY",
    "KRW",
    "LTL",
    "MYR",
    "NOK",
    "PHP",
    "PLN",
    "RON",
    "RUB",
    "RSD",
    "SEK",
    "THB",
    "TRY",
    "UAH",
    "USD",
    "VND",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            title: Text(
              'Currency',
              style: TextStyle(color: Colors.black),
            ),
            leading: new IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                    (route) => false);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: ListView.builder(
          itemCount: currency.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: ListTile(
                tileColor: Colors.white,
                title: Text(
                  currency[index],
                  style: TextStyle(
                    color: Constants.kitGradients[12],
                    fontSize: screenWidth(context, dividedBy: 25),
                  ),
                ),
                onTap: () {
                  value = currency[index];
                  Hive.box('code').put(2, value);
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomeScreen()),
                      (route) => false);
                },
              ),
            );
          }),
    );
  }
}
