import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_3/localization/locale.dart';
import 'package:hotel_booking_3/src/ui/screens/search_destination.dart';
import 'package:hotel_booking_3/src/ui/screens/webview.dart';
import 'package:hotel_booking_3/src/ui/widgets/calender.dart';
import 'package:hotel_booking_3/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:hotel_booking_3/src/ui/widgets/home_drawer.dart';
import 'package:hotel_booking_3/src/ui/widgets/home_page_field_box.dart';
import 'package:hotel_booking_3/src/ui/widgets/room_person_count.dart';
import 'package:hotel_booking_3/src/utils/constants.dart';
import 'package:hotel_booking_3/src/utils/get_month_alpha.dart';
import 'package:hotel_booking_3/src/utils/object_factory.dart';
import 'package:hotel_booking_3/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'destination_page.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  var destination = "Select destination";
  String dates;
  String guest;
  String checkInDate;
  String checkOutDate;
  String nights;
  int rooms=0;
  int adults=0;
  int children=0;
  List<int> adultArray;
  List<int> childrenArray;
  SharedPreferences _sharedPreferences;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseAuth _authfb = FirebaseAuth.instance;
  String monthInLetterCheckIn;
  String monthInLetterCheckOut;
  String weekdayInLetterCheckIn;
  String weekdayInLetterCheckOut;
  AnimationController animationController;
  Animation animationSize;

  int counterValueAdults1=2;
  int counterValueAdults2=0;
  int counterValueAdults3=0;
  int counterValueAdults4=0;
  int counterValueAdults5=0;
  int counterValueAdults6=0;
  int counterValueAdults7=0;
  int counterValueAdults8=0;
  int counterValueAdults9=0;
  int counterValueAdultsSum=0;

  int counterValueChildren1=0;
  int counterValueChildren2=0;
  int counterValueChildren3=0;
  int counterValueChildren4=0;
  int counterValueChildren5=0;
  int counterValueChildren6=0;
  int counterValueChildren7=0;
  int counterValueChildren8=0;
  int counterValueChildren9=0;
  int counterValueChildrenSum=0;

  String adultCountUnit="",childrenCountUnit="";

  void getSharedPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    if(Hive.box('adult').get(1)==null)
    Hive.box('adult').put(1, 1);

    counterValueAdults1=Hive.box('adult').get(21) != null?Hive.box('adult').get(21):counterValueAdults1;
    counterValueAdults2=Hive.box('adult').get(21) != null?Hive.box('adult').get(22):counterValueAdults2;
    counterValueAdults3=Hive.box('adult').get(21) != null?Hive.box('adult').get(23):counterValueAdults3;
    counterValueAdults4=Hive.box('adult').get(21) != null?Hive.box('adult').get(24):counterValueAdults4;
    counterValueAdults5=Hive.box('adult').get(21) != null?Hive.box('adult').get(25):counterValueAdults5;
    counterValueAdults6=Hive.box('adult').get(21) != null?Hive.box('adult').get(26):counterValueAdults6;
    counterValueAdults7=Hive.box('adult').get(21) != null?Hive.box('adult').get(27):counterValueAdults7;
    counterValueAdults8=Hive.box('adult').get(21) != null?Hive.box('adult').get(28):counterValueAdults8;
    counterValueAdults9=Hive.box('adult').get(21) != null?Hive.box('adult').get(29):counterValueAdults9;

    counterValueAdultsSum=counterValueAdults1+counterValueAdults2+counterValueAdults3+counterValueAdults4+counterValueAdults5+counterValueAdults6+counterValueAdults7+counterValueAdults8+counterValueAdults9;
    adultCountUnit=counterValueAdultsSum>1?"Adults":"Adult";

    counterValueChildren1=Hive.box('adult').get(31) != null?Hive.box('adult').get(31):counterValueChildren1;
    counterValueChildren2=Hive.box('adult').get(32) != null?Hive.box('adult').get(32):counterValueChildren2;
    counterValueChildren3=Hive.box('adult').get(33) != null?Hive.box('adult').get(33):counterValueChildren3;
    counterValueChildren4=Hive.box('adult').get(34) != null?Hive.box('adult').get(34):counterValueChildren4;
    counterValueChildren5=Hive.box('adult').get(35) != null?Hive.box('adult').get(35):counterValueChildren5;
    counterValueChildren6=Hive.box('adult').get(36) != null?Hive.box('adult').get(36):counterValueChildren6;
    counterValueChildren7=Hive.box('adult').get(37) != null?Hive.box('adult').get(37):counterValueChildren7;
    counterValueChildren8=Hive.box('adult').get(38) != null?Hive.box('adult').get(38):counterValueChildren8;
    counterValueChildren9=Hive.box('adult').get(39) != null?Hive.box('adult').get(39):counterValueChildren9;

    counterValueChildrenSum=counterValueChildren1+counterValueChildren2+counterValueChildren3+counterValueChildren4+counterValueChildren5+counterValueChildren6+counterValueChildren7+counterValueChildren8+counterValueChildren9;
    childrenCountUnit=counterValueChildrenSum>1?"Children":"Child";

    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    animationSize = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 2.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 2.0, end: 1.0), weight: 1)
    ]).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.8, 1.0)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();


    setState(() {
      monthInLetterCheckIn = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(5));
    });
    setState(() {
      monthInLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(6));
    });
    if (Hive.box('weekDay').get(32) != null) {
      setState(() {
        weekdayInLetterCheckIn = ObjectFactory()
            .getWeekDayAlpha
            .getCheckInWeek(Hive.box('weekDay').get(32).toString());
      });
    } else if (Hive.box('weekDay').get(32) == null) {
      setState(() {
        weekdayInLetterCheckIn = "week day";
      });
    }
    if (Hive.box('weekDay').get(33) != null) {
      setState(() {
        weekdayInLetterCheckOut = ObjectFactory()
            .getWeekDayAlpha
            .getCheckInWeek(Hive.box('weekDay').get(33).toString());
      });
    } else if (Hive.box('weekDay').get(33) == null) {
      setState(() {
        weekdayInLetterCheckOut = "week day";
      });
    }

    if (Hive.box('adult').get(5) != null) {
      setState(() {
        nights = Hive.box('adult').get(5).toString();
      });
    } else {
      nights = "No: of";
    }
    if (Hive.box('code').get(3) != null) {
      setState(() {
        checkInDate =
            Hive.box('code').get(3).toString() + " " + monthInLetterCheckIn;
      });
    } else {
      setState(() {
        checkInDate = (DateTime.now().day.toString())+" "+(GetMonthAlpha().getCheckInMonth(DateTime.now().month.toString()));
      });
    }
    if (Hive.box('code').get(6) != null) {
      setState(() {
        checkOutDate =
            Hive.box('code').get(4).toString() + " " + monthInLetterCheckOut;
      });
    } else {
      setState(() {
        checkOutDate = (DateTime.now().add(Duration(days: 1)).day.toString())+" "+(GetMonthAlpha().getCheckInMonth(DateTime.now().add(Duration(days: 1)).month.toString()));;
      });
    }

    if (Hive.box('adult').get(1) !=null) {
      setState(() {
        rooms = Hive.box('adult').get(1);
      });
    } else {
      setState(() {
        rooms = 1;
      });
    }

     ////////////////////////////////////////////////////////////////////

    if (Hive.box('adult').get(2) != null) {
      setState(() {
        adultArray = Hive.box('adult').get(2);
        for(int i=0;i<adultArray.length;i++){
          adults=adults + adultArray[i];
        }
      });
    } else {
      setState(() {
        adults = 1;
      });
    }

    ////////////////////////////////////////////////////////////////////

    if (Hive.box('adult').get(3) != null) {
      setState(() {
        childrenArray = Hive.box('adult').get(3);
        for(int i=0;i<childrenArray.length;i++){
          children = children + childrenArray[i];
        }
      });
    } else {
      setState(() {
        children = 0;
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          resizeToAvoidBottomPadding: false,
            key: _globalKey,
            backgroundColor: Constants.kitGradients[3],
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(0.0), // here the desired height
                child: AppBar(
                  backgroundColor: Constants.kitGradients[3],
                  elevation: 0,
                )),
            drawer: HomeDrawer(auth: _auth, authfb: _authfb),
            floatingActionButton: GestureDetector(
              child: Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  height: screenHeight(context, dividedBy: 13),
                  decoration: BoxDecoration(
                      color: Constants.kitGradients[12],
                      borderRadius: BorderRadius.circular(26.0)),
                  child: Center(
                    child: Text(
                      'Search for Hotels',
                      style: TextStyle(
                          color: Constants.kitGradients[13],
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          fontFamily: 'Poppins'),
                    ),
                  )),
              onTap: () {
                if (Hive.box('lang').get(10) != null) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Webview()));
                } else {
                  final snackbar = SnackBar(
                    content: Text(
                      Constants.CHOOSE_DESTINATION,
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          fontSize: 14.0),
                    ),
                    duration: Duration(seconds: 3),
                    backgroundColor: Constants.kitGradients[0],
                  );
                  _globalKey.currentState.showSnackBar(snackbar);
                }
              },
            ),
            body: Builder(
              builder: (context) => SafeArea(
                top: true,
                left: true,
                bottom: true,
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: screenHeight(context, dividedBy: 18),
                              left: screenHeight(context, dividedBy: 27)),
                          child: GestureDetector(
                            child: SvgPicture.asset('assets/images/menu_icon.svg'),
                            onTap: () {
                              _globalKey.currentState.openDrawer();
                            },
                          )),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 60),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 15),
                        ),
                        Text(
                          getTranslated(context, 'Search'),
                          style: TextStyle(
                              fontSize: 30.0,
                              color: Constants.kitGradients[14],
                              fontWeight: FontWeight.w300,
                              fontFamily: 'Gilroy',
                              fontStyle: FontStyle.normal),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 80),
                        ),
                        Text(
                          getTranslated(context, 'Hotels'),
                          style: TextStyle(
                              fontSize: 30.0,
                              fontWeight: FontWeight.w800,
                              fontFamily: 'Gilroy',
                              color: Constants.kitGradients[14],
                              fontStyle: FontStyle.normal),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 60),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 1.2),
                        height: screenHeight(context, dividedBy: 11),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            color: Constants.kitGradients[3],
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFDBE7EB).withOpacity(0.2),
                                  offset: Offset(0, 5),
                                  spreadRadius: 1.0,
                                  blurRadius: 1.0)
                            ]),
                        child: Center(
                          child: TextFormField(
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        DestinationPage()),
                              );
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                              icon: Icon(
                                  Icons.search,
                                  size: 30,
                                  color: Constants.kitGradients[6],
                                ),

                              hintText: Hive.box('lang').get('place') != null
                                  ? Hive.box('lang').get('place')+", "+Hive.box('lang').get('country')
                                  : getTranslated(context, 'select_destination'),
                              hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'Poppins',
                                  color: Constants.kitGradients[16]),
                              border: InputBorder.none,
                            ),
                          ),
                        )),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 1.2),
                        height: screenHeight(context, dividedBy: 5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            color: Constants.kitGradients[3],
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFDBE7EB).withOpacity(0.2),
                                  offset: Offset(0, 5),
                                  spreadRadius: 1.0,
                                  blurRadius: 1.0)
                            ]),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text(
                                      'Check-in',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[15],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      "$checkInDate",
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[16],
                                          fontSize: 22,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      weekdayInLetterCheckIn,
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[15],
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 0, top: 10),
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Constants.kitGradients[12],
                                  ),
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DatePicker()),
                                        );
                                      },
                                      child: Image(
                                          image: AssetImage(
                                              "assets/images/Calender.png"),
                                          height: 70,
                                          width: 70)),
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Center(
                                    child: Text(
                                      'Check-out',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[15],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      "$checkOutDate",
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[16],
                                          fontSize: 22,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  ),
                                  Center(
                                    child: Text(
                                      weekdayInLetterCheckOut,
                                      style: TextStyle(
                                          fontFamily: 'NotoSans',
                                          color: Constants.kitGradients[15],
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 20),
                    ),
                    GestureDetector(
                      child: Row(
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 15),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Constants.kitGradients[1],
                            ),
                            child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 8.0, horizontal: 8.0),
                                child: SvgPicture.asset(
                                  'assets/images/person.svg',
                                )),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 22),
                          ),
                          Column(
                            children: [
                              Text(
                                'Rooms',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Constants.kitGradients[2],
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'NotoSans'),
                              ),
                              Row(
                                children: [
                                  SvgPicture.asset(
                                      "assets/icons/dec_counter.svg"),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(rooms.toString(),
                                      style: TextStyle(
                                          color: Constants.kitGradients[2],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'NotoSans')),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  SvgPicture.asset(
                                      "assets/icons/inc_counter.svg"),
                                ],
                              )
                            ],
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 22),
                          ),
                          Column(
                            children: [
                              Text(
                                'Guests',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Constants.kitGradients[2],
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'NotoSans'),
                              ),
                              Row(
                                children: [
                                  Text(
                                    counterValueAdultsSum.toString()+' '+adultCountUnit +'-' +counterValueChildrenSum.toString()+" "+childrenCountUnit,
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Constants.kitGradients[17],
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                        fontFamily: 'NotoSans'),
                                  ),
                                  Icon(Icons.arrow_drop_down_outlined)
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>ModalBottomSheet()));
                      },
                    ),
                  ],
                ),
              ),
            ));
  }
}
